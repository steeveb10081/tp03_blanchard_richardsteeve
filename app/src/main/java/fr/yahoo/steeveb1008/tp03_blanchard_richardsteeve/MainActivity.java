package fr.yahoo.steeveb1008.tp03_blanchard_richardsteeve;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import static android.Manifest.permission.CALL_PHONE;

public class MainActivity extends AppCompatActivity {

    ImageButton buttMail ;
    ImageButton buttCall ;
    ImageButton buttWeb ;
    ImageButton buttTp01 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttMail =findViewById(R.id.buttMail);
        buttCall =findViewById(R.id.buttCall);
        buttWeb =findViewById(R.id.buttWeb);
        buttTp01 =findViewById(R.id.buttTp01);

        buttMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mailIntent = new Intent();
                mailIntent.setAction(Intent.ACTION_SEND);
                mailIntent.putExtra(Intent.EXTRA_TEXT, "GOD My life");
                mailIntent.setType("text/plain");
                try {
                    startActivity(mailIntent);
                } catch (ActivityNotFoundException e) {

                }
            }
        });

        buttCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent();
                callIntent.setData(Uri.parse("tel:"+ 31112627));
                if (ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    callIntent.setAction(Intent.ACTION_CALL);
                    startActivity(callIntent);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{CALL_PHONE}, 1);
                    }else{
                        callIntent.setAction(Intent.ACTION_DIAL);
                        startActivity(callIntent);
                    }
                }
            }
        });

        buttWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.google.com";
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(url));
                startActivity(webIntent);
            }
        });

        buttTp01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent webIntent = new Intent("mbds.haiti.TOAST");
                startActivity(webIntent);
            }
        });

    }
}